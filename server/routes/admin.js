/**
 * Created by starsky on 5/12/17.
 */
const _ = require('lodash');
const {mongoose} = require('./../db/mongoose');
const {Admin} = require('./../models/adminModel');
const {authenticateAdminSignUp} = require('./../middleware/authenticateAdminSignUp');

const {GameBuildings} = require('./../models/game-buildingsModel');

const admin = require('express').Router();

admin.get('/buildings', function (req, res) {
    GameBuildings.distinct('buildings', (err, buildings) => {
        return buildings;
    }).lean().then((buildings) => {
        res.send(buildings);
    });
});

admin.get('/sign-up', (req, res) => {
    res.render('admin-signUp.hbs', {
        action: '/sign-up',
        button: 'Sign Up',
        email: 'email',
        key: 'key',
        name: 'login',
        title: 'SuperBarrio Analytics Admin Sign Up',
        password: 'password'
    });
});

admin.post('/sign-up', authenticateAdminSignUp, (req, res) => {
    let body = _.pick(req.body, ['email', 'password', 'key']);
    let admin = new Admin({
        email: body.email,
        password: body.password
    });
    admin.save().then((admin) => {
        return admin.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token);
        res.redirect(`/admin/sign-up`);
    }).catch((e) => {
        res.status(400).send(e);
    });


});

module.exports = admin;