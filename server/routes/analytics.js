/**
 * Created by starsky on 5/12/17.
 */
const _ = require('lodash');

const {mongoose} = require('./../db/mongoose');

const analytics = require('express').Router();

analytics.get('/', (req, res) => {
    res.render('analytics-login.hbs', {
        action: '/analytics-dashboard.html',
        button: 'Login',
        email: 'email',
        name: 'login',
        title: 'SuperBarrio Analytics',
        password: 'password'
    });
});


module.exports = analytics;