/**
 * Created by starsky on 5/12/17.
 */
const {mongoose} = require('./../db/mongoose');
const {Player} = require('./../models/playerModel');
const {GameBuildings} = require('./../models/game-buildingsModel');
const {GameScore} = require('./../models/game-scoreModel');

const {isRealString} = require('./../utils/validation');

const game = require('express').Router();

game.get('/game-upload', (req, res) => {
    res.render('game-upload', {
        name: 'game',
        action: '/game/game-upload',
        title: 'SuperBarrio Analytics Upload-Game',
        dataGame: 'json'
    });
});

game.post('/game-upload', (req, res) => {
    let game = JSON.parse(req.body.json);
    let gameData = JSON.stringify({buildingData: game.buildingData, counterData: game.counterData});

    let player = new Player({
        actions: game.playerData.PlayerActions,
        age: Number(game.playerData.PlayerAge),
        gender: game.playerData.PlayerGender,
        score: Number(game.playerData.PlayerScore),
        name: game.playerData.PlayerName,
        email: `${game.playerData.PlayerName}@iaac.net`,
        password: game.playerData.PlayerName
    });

    player.save().then((player) => {
        return player.generateAuthToken();
    }).then((token) => {
        res.header('x-auth', token);
        res.redirect(`/game/game-buildings/:${player._id}/:${gameData}`);
    }).catch((e) => {
        res.status(400).send(e);
    });
});

game.get('/game-buildings/:id/:GameData', (req, res) => {
    if (!isRealString(req.params.GameData) || !isRealString(req.params.id)) {
        res.status(400);
    }
    let gameData;
    let gameId = (req.params.id).slice(1);
    try {
        gameData = JSON.parse(`{ "data" ${req.params.GameData} }`);
    } catch (e) {
        res.status(400).send(e);
    }
    let gameBuildings = new GameBuildings({
        buildings: gameData.data.buildingData,
        _creator: gameId,
    });

    gameBuildings.save().then(() => {
        res.redirect(`/game/game-score/:${gameId}/:${JSON.stringify(gameData.data)}`);

    }, (e) => {
        res.status(400).send(e);
    });
});

game.get('/game-score/:id/:GameData', (req, res) => {
    if (!isRealString(req.params.GameData) || !isRealString(req.params.id)) {
        res.status(400);
    }
    let gameData;
    let gameId = (req.params.id).slice(1);
    try {
        gameData = JSON.parse(`{ "data" ${req.params.GameData} }`);
    } catch (e) {
        res.status(400).send(e);
    }

    let gameScore = new GameScore({
        scores: gameData.data.counterData,
        _creator: gameId
    });

    gameScore.save().then(() => {
        res.redirect(`/game/game-upload`);

    }, (e) => {
        res.status(400).send(e);
    });
});

module.exports = game;