/**
 * Created by starsky on 5/11/17.
 */
require('./config/config.js');
const path = require('path');
const http = require('http');

const express = require('express');
const socketIO = require('socket.io');
const bodyParser = require('body-parser');
const hbs = require('hbs');
const async = require('async');

const {isRealString} = require('./utils/validation');

const {evenEmitterBuildings} = require('./utils/eventEmitter');

const {DashboardUsers} = require('./utils/dashboard-users');

const {DashboardGameBuildings} = require('./utils/dashboard-gameBuildings');

const {DashboardGameBuildingsGroups} = require('./utils/dashboard-gameBuildingsGroups');


const port = process.env.PORT;

let app = express();
let server = http.createServer(app);
let io = socketIO(server);

let game = require('./routes/game');
let admin = require('./routes/admin');
let analytics = require('./routes/analytics');

app.use(express.static(path.join(__dirname, '../public')));
app.use(bodyParser.urlencoded({extended: true}));
app.set('view engine', 'hbs');

app.use('/game', game);
app.use('/admin', admin);
app.use('/', analytics);

let dashboardUsers = new DashboardUsers();
let dashboardGameBuildings = new DashboardGameBuildings();
let dashboardGameBuildingsGroups = new DashboardGameBuildingsGroups();


io.on('connection', (socket) => {
    console.info(`New Admin connection`);

    dashboardGameBuildingsGroups.getDashboardGameBuildingsGroups();
    dashboardGameBuildings.getDashboardGameBuildings();

    socket.on('join', (params, callback) => {
        if (!isRealString(params.email)) {
            return callback('User is required')
        }
        //change for room
        socket.join(params.email);
        dashboardUsers.removeDashboardUser(socket.id);
        dashboardUsers.addDashboardUser(socket.id, params.email, params.email);

        let filter = "all";
        io.to(params.email).emit('update_DashboardUsers', dashboardUsers.getDashboardUserList(params.email));
        io.to(params.email).emit('update_DashboardBuildings', dashboardGameBuildings.getDashboardGameBuildingsGender(filter, 10, 4, 2));
        io.to(params.email).emit('update_DashboardScores', dashboardGameBuildingsGroups.getDashboardGameBuildingsScoresCounterScores(filter), filter);
    });

    socket.on('allData', (filter) => {
        dashboardUser = dashboardUsers.getDashboardUser(socket.id);
        io.to(dashboardUser.room).emit('update_DashboardBuildings', dashboardGameBuildings.getDashboardGameBuildingsGender(filter, 10, 4, 2));
        io.to(dashboardUser.room).emit('update_DashboardScores', dashboardGameBuildingsGroups.getDashboardGameBuildingsScoresCounterScores(filter), filter);
    });

    evenEmitterBuildings.on('newPush', (message, data) => {
        console.info(message);
        dashboardUser = dashboardUsers.getDashboardUser(socket.id);
        io.to(dashboardUser.room).emit('update_DashboardBuildings', dashboardGameBuildings.getDashboardGameBuildingsGender(filter, 10, 4, 2));
        io.to(dashboardUser.room).emit('update_DashboardScores', dashboardGameBuildingsGroups.getDashboardGameBuildingsScoresCounterScores(filter), filter);
    });

    socket.on('disconnect', () => {
        let dashboardUser = dashboardUsers.removeDashboardUser(socket.id);
        if (dashboardUser) {
            io.to(dashboardUser.room).emit('updateDashboardUsersList', dashboardUsers.getDashboardUserList(dashboardUser.email));
        }
        console.info('User was disconnect from the server');
    });
})
;

server.listen(port, () => {
    console.log(`Started at port ${port}`);
});

module.exports = {app};
