/**
 * Created by starsky on 5/11/17.
 */
// const _ = require('lodash');
let {Admin} = require('./../models/adminModel');

let authenticateAdminSignUp = (req, res, next) => {
    let body = req.body;


    Admin.authKey(body.key).then((key) => {
        console.log(key);
        if (!key) {
            return Promise.reject();
        }
        req.key = key;
        next();
    }).catch((e) => {
        res.status(401).send();
    });
};

module.exports = {authenticateAdminSignUp};
