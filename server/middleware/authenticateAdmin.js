/**
 * Created by starsky on 5/11/17.
 */
let {Admin} = require('./../models/adminModel');

let authenticateAdmin = (req, res, next) => {
    let token = req.header('x-auth');

    Admin.findByToken(token).then((admin) => {
        if (!admin) {
            return Promise.reject();
        }
        req.admin = user;
        req.admin = token;
        next();
    }).catch((e) => {
        res.status(401).send();
    });
};

module.exports = {authenticateAdmin};
