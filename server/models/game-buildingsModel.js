/**
 * Created by starsky on 5/11/17.
 */
const mongoose = require('mongoose');
const {evenEmitterBuildings} = require('./../utils/eventEmitter');

let GameBuildingsSchema = new mongoose.Schema({
    buildings: [
        {
            _type:{
                default: 'building',
                lowercase: true,
                required: true,
                trim: true,
                type: String
            },
            title: {
                lowercase: true,
                minlength: 1,
                required: true,
                trim: true,
                type: String
            },
            x: {
                required: true,
                type: Number
            },
            y: {
                required: true,
                type: Number
            },
            _createdAt: {
                type: Date,
                default: Date.now
            },
        }
    ],
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});

GameBuildingsSchema.methods.toJSON = function () {
    let buildings = this;
    return buildings.toObject();
};

GameBuildingsSchema.methods.list = function () {
    let GameBuildings = this;

    return GameBuildings.distinct("buildings", (err, buildings) => {
        return buildings;
    }).lean().then((buildings) => {
        return buildings;
    });
};

GameBuildingsSchema.post('save', function () {
    evenEmitterBuildings.emit('newPush', '__ uploading new buildings' ,true )
});

let GameBuildings = mongoose.model('GameBuildings', GameBuildingsSchema);

module.exports = {GameBuildings};