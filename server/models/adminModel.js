/**
 * Created by starsky on 5/11/17.
 */
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const bcrypt = require('bcryptjs');

let AdminSchema = new mongoose.Schema({
    email: {
        lowercase: true,
        minlength: 1,
        trim: true,
        unique:false,
        type: String,
        validate: {
            validator: (value) => {
                return validator.isEmail(value);
            },
            message: `{VALUE} is not a valid mail`
        }
    },
    password: {
        minlength: 1,
        type: String
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            required: true,
            type: String
        }
    }],
    _createdAt: {
        type: Date,
        default: Date.now
    },
    _login: {
        type: Number,
        default: 0
    }
});

AdminSchema.methods.toJSON = function () {
    let admin = this;
    return admin.toObject();
};

AdminSchema.methods.generateAuthToken = function () {
    let admin = this;
    let access = 'auth';
    let token = jwt.sign({_id: admin._id.toHexString(), access}, process.env.JWT_SECRET).toString();

    admin.tokens.push({access, token});
    console.log(({access, token}));

    return admin.save().then(() => {
        return token;
    })
};

AdminSchema.methods.removeToken = function (token) {
    let admin = this;

    return admin.update({$pull: {tokens: {token}}});
};

AdminSchema.methods.findByToken = function (token) {
    let Admin = this;
    let decode;

    try {
        decode = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject();
    }

    return Admin.findOne({
        '_id': decode._id,
        'tokens.access': 'auth',
        'tokens.token': token
    });
};

AdminSchema.statics.updateByToken = function (token) {
    let decode;
    try {
        decode = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject();
    }
    return Admin.findOneAndUpdate({
        '_id': decode._id,
        'tokens.access': 'auth',
        'tokens.token': token
    }, { $inc: { _login : 1 } });
};

AdminSchema.statics.findByCredentials = function (email, password) {
    let Admin = this;

    return Admin.findOne({email}).then((admin) => {
        if (!admin) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, admin.password, (err, res) => {
                if (res) {
                    resolve(admin);
                } else {
                    reject();
                }
            })
        })
    })
};

AdminSchema.statics.authKey = function (key) {
    return new Promise((resolve, reject) => {
        if (key !== '2Vc86SkrMChRYqCV') {
            reject();
        } else {
            resolve(() => {
                return key;
            });
        }
    });
};

AdminSchema.pre('save', function (next) {
    let admin = this;
    if (admin.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(admin.password, salt, (err, hash) => {
                admin.password = hash;
                next();
            })
        })
    } else {
        next();
    }
});

let Admin = mongoose.model('Admin', AdminSchema);
module.exports = {Admin};
