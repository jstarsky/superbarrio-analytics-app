/**
 * Created by starsky on 5/11/17.
 */
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const validator = require('validator');
const bcrypt = require('bcryptjs');

let PlayerSchema = new mongoose.Schema({
    actions: {
        type: Array
    },
    age: {
        default: 9,
        type: Number,
        min: 9
    },
    gender: {
        enum: ['male', 'female'],
        lowercase: true,
        type: String
    },
    score: {
        default: 0,
        type: Number
    },
    name: {
        lowercase: true,
        minlength: 1,
        trim: true,
        required: true,
        type: String
    },
    email: {
        lowercase: true,
        minlength: 1,
        trim: true,
        type: String,
        unique: true,
        validate: {
            validator: (value) => {
                return validator.isEmail(value);
            },
            message: `{VALUE} is not a valid mail`
        }
    },
    password: {
        minlength: 1,
        required: true,
        type: String
    },
    tokens: [{
        access: {
            type: String,
            required: true
        },
        token: {
            required: true,
            type: String
        }
    }],
    _createdAt: {
        type: Date,
        default: Date.now
    },
});

PlayerSchema.methods.toJSON = function () {
    let player = this;
    return player.toObject();
};

PlayerSchema.methods.generateAuthToken = function () {
    let player = this;
    let access = 'auth';
    let token = jwt.sign({_id: player._id.toHexString(), access}, process.env.JWT_SECRET).toString();

    player.tokens.push({access, token});

    return player.save().then(() => {
        return token;
    })
};

PlayerSchema.methods.removeToken = function (token) {
    let player = this;

    return player.update({$pull: {tokens: {token}}});
};

PlayerSchema.methods.findByToken = function (token) {
    let Player = this;
    let decode;

    try {
        decode = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject();
    }

    return Player.findOne({
        '_id': decode._id,
        'tokens.access': 'auth',
        'tokens.token': token
    });
};

PlayerSchema.statics.findByCredentials = function (email, password) {
    let Player = this;

    return Player.findOne({email}).then((player) => {
        if (!player) {
            return Promise.reject();
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(password, player.password, (err, res) => {
                if (res) {
                    resolve(player);
                } else {
                    reject();
                }
            })
        })
    })
};

PlayerSchema.pre('save', function (next) {
    let player = this;
    if (player.isModified('password')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(player.password, salt, (err, hash) => {
                player.password = hash;
                next();
            })
        })
    } else {
        next();
    }
});

let Player = mongoose.model('Player', PlayerSchema);
module.exports = {Player};
