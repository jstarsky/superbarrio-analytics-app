/**
 * Created by starsky on 5/11/17.
 */
const mongoose = require('mongoose');

let GameScoreSchema = new mongoose.Schema({
    scores: [{
        _type:{
            default: 'score',
            lowercase: true,
            required: true,
            trim: true,
            type: String
        },
        accessibility: {
            default: 0,
            type: Number
        },
        ecology: {
            default: 0,
            type: Number
        },
        economy: {
            default: 0,
            type: Number
        },
        productivity: {
            default: 0,
            type: Number
        },
        socialCulture: {
            default: 0,
            type: Number
        }
    }],
    _creator: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    _createdAt: {
        type: Date,
        default: Date.now
    },
});

GameScoreSchema.pre('save', function (next) {
    let gameScore = this;

    if (gameScore.isNew && 0 === gameScore.scores.length) {
        gameScore.isNew = undefined;
    }
    next();
});

GameScoreSchema.methods.toJSON = function () {
    let gameScore = this;
    return gameScore.toObject();
};

let GameScore = mongoose.model('GameScore', GameScoreSchema);
module.exports = {GameScore};