/**
 * Created by starsky on 5/11/17.
 */
let isRealString = (str) => {
    return typeof  str === 'string' && str.trim().length > 0;
};

module.exports = {isRealString};