/**
 * Created by starsky on 5/17/17.
 */

let scoreStyle =
    {
        accessibility: {
            _general:{
                _type: "accessibility",
                hex: "#F56991",
                RGB: "245,105,145"
            },
            bike: {
                _type: "bike",
                hex: "#f4ada4",
                RGB: "244,127,122"
            },
            bus: {
                _type: "bus",
                hex: "#f53c49",
                RGB: "245,105,145"
            },
            drone: {
                _type: "drone",
                hex: "#B2666D",
                RGB: "178,102,10"
            },
            car: {
                _type: "car",
                hex: "#745162",
                RGB: "116,81,98"
            },
        },
        productivity: {
            _general:{
                _type: "productivity",
                hex: "#EFFAB4",
                RGB: "239,250,180"
            },
            solarEnergy: {
                _type: "solarEnergy",
                hex: "#faa14f",
                RGB: "239,250,180"
            },
            windEnergy: {
                _type: "windEnergy",
                hex: "#88faf3",
                RGB: "239,250,180"
            },
            bioMass: {
                _type: "bioMass",
                hex: "#fa4289",
                RGB: "239,250,180"
            },
            seaSaw: {
                _type: "seaSaw",
                hex: "#97a8fa",
                RGB: "239,250,180"
            },

        },
        socialCulture: {
            _general:{
                _type: "socialCulture",
                hex: "#FA6900",
                RGB: "250,105,0"
            },
            furniture: {
                _type: "furniture",
                hex: "#EAB05E",
                RGB: "234,176,94"
            },
            publicBuilding: {
                _type: "publicBuilding",
                hex: "#FA6900",
                RGB: "250,105,0"
            },
            publicInfrastructure: {
                _type: "publicInfrastructure",
                hex: "#B8DE6A",
                RGB: "184,222,106"
            }
        },
        ecology: {
            _general:{
                _type: "ecology",
                hex: "#77a469",
                RGB: "124,164,10"
            },
            nature: {
                _type: "nature",
                hex: "#7CA40A",
                RGB: "124,164,10"
            },
            publicSpace: {
                _type: "publicSpace",
                hex: "#a1a467",
                RGB: "124,164,10"
            },
            food: {
                _type: "food",
                hex: "#FA6900",
                RGB: "124,164,10"
            }
        }
    };

module.exports = {scoreStyle};
