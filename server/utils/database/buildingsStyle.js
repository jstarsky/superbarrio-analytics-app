/**
 * Created by starsky on 5/17/17.
 */

let {scoreStyle} = require('./scoreStyle');

//ecology
let nature = scoreStyle.ecology.nature.hex;
let publicSpace = scoreStyle.ecology.publicSpace.hex;
let food = scoreStyle.ecology.food.hex;

//accessibility
let bike = scoreStyle.accessibility.bike.hex;
let bus = scoreStyle.accessibility.bus.hex;
let car = scoreStyle.accessibility.car.hex;
let drone = scoreStyle.accessibility.drone.hex;

//productivity
let solarEnergy = scoreStyle.productivity.solarEnergy.hex;
let windEnergy = scoreStyle.productivity.solarEnergy.hex;
let bioMass = scoreStyle.productivity.bioMass.hex;
let seaSaw = scoreStyle.productivity.seaSaw.hex;

//socialCulture
let furniture = scoreStyle.socialCulture.furniture.hex;
let publicBuilding = scoreStyle.socialCulture.publicBuilding.hex;
let publicInfrastructure = scoreStyle.socialCulture.publicInfrastructure.hex;

let buildingsStyle = [
    // ecology
    {
        title: "tree",
        _title: "tree",
        _score: {
            _type: "ecology"
        },
        color: {
            hex: nature
        }
    },
    {
        title: "park",
        _title: "park",

        _score: {
            _type: "ecology"
        },
        color: {
            hex: publicSpace
        }
    },
    {
        title: "food market",
        _title: "foodMarket",

        _score: {
            _type: "ecology"
        },
        color: {
            hex: food
        }
    },
    {
        title: "vertical garden",
        _title: "verticalGarden",
        _score: {
            _type: "ecology"
        },
        color: {
            hex: nature
        }
    },
    {
        title: "drinking fountain",
        _title: "drinkingFountain",
        _score: {
            _type: "ecology"
        },
        color: {
            hex: publicSpace
        }
    },
    {
        title: "urban orchard",
        _title: "urbanOrchard",
        _score: {
            _type: "ecology"
        },
        color: {
            hex: food
        }
    },
    // socialCulture
    {
        title: "barbecue",
        _title: "barbecue",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: furniture
        }
    },
    {
        title: "library",
        _title: "library",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicBuilding
        }
    },
    {
        title: "trampoline",
        _title: "trampoline",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: furniture
        }
    },
    {
        title: "water park",
        _title: "waterPark",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicInfrastructure
        }
    },
    {
        title: "amphitheatre",
        _title: "amphitheatre",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicBuilding
        }
    },
    {
        title: "bench",
        _title: "bench",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: furniture
        }
    },
    {
        title: "library(clone)",
        _title: "libraryClone",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicBuilding
        }
    },
    {
        title: "wifi spot",
        _title: "wifiSpot",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: furniture
        }
    },
    {
        title: "piso technology",
        _title: "TechnologyBuilding",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicBuilding
        }
    },
    {
        title: "skateboard station",
        _title: "skateboardStation",
        _score: {
            _type: "socialCulture"
        },
        color: {
            hex: publicBuilding
        }
    },
    // accessibility
    {
        title: "carpark",
        _title: "carpark",
        _score: {
            _type: "accessibility"
        },
        color: {
            hex: car
        }
    },
    {
        title: "bike station",
        _title: "bikeStation",
        _score: {
            _type: "accessibility"
        },
        color: {
            hex: bike
        }
    },
    {
        title: "bus station",
        _title: "busStation",
        _score: {
            _type: "accessibility"
        },
        color: {
            hex: bus
        }
    },
    {
        title: "drone landing bay",
        _title: "droneLandingBay",
        _score: {
            _type: "accessibility"
        },
        color: {
            hex: drone
        }
    },
    // energy
    {
        title: "sea-saw generator",
        _title: "seaSawGenerator",
        _score: {
            _type: "productivity"
        },
        color: {
            hex: seaSaw
        }
    },
    {
        title: "solar charging station",
        _title: "solarChargingStation",
        _score: {
            _type: "productivity"
        },
        color: {
            hex: solarEnergy
        }
    },
    {
        title: "biomass generator",
        _title: "biomassGenerator",
        _score: {
            _type: "productivity"
        },
        color: {
            hex: bioMass
        }
    },
    {
        title: "vertical wind turbine",
        _title: "verticalWindTurbine",
        _score: {
            _type: "productivity"
        },
        color: {
            hex: windEnergy
        }
    }
];


module.exports = {buildingsStyle};
