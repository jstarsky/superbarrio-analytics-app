/**
 * Created by starsky on 5/14/17.
 */
const mongo = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
let db;

mongo.connect(process.env.MONGODB_URI.toString(), {auto_reconnect: true}, function (err, database) {
    if (err) throw err;
    db = database;
    console.log(`data base open...`);
});

exports.findAllBuildings_ = function () {
    return new Promise(function (resolve, reject) {

        db.collection('gamebuildings').distinct("buildings", (err, buildings) => {
            if (err) {
                reject(err);
            } else {
                resolve(buildings);
            }
        });
    });
};

exports.findAllBuildings = function () {
    return new Promise(function (resolve, reject) {

        db.collection('gamebuildings').find({}, (err, buildings) => {

            if (err) {
                reject(err);
            } else {
                resolve(buildings);
            }
        });
    });
};

exports.findPlayerId = function (id) {
    return new Promise(function (resolve, reject) {
        db.collection('players').find({_id: ObjectId(id.toString())}, (err, player) => {
            if (err) {
                reject(err);
            } else {
                resolve(player);
            }
        });
    });
};
