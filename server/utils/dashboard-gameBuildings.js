/**
 * Created by starsky on 5/14/17.
 */
const _ = require('lodash');
let mongoConnect = require('./mongoConnect');

const {buildingsStyle} = require('./database/buildingsStyle');

class DashboardGameBuildings {
    constructor() {
        this.dashboardGameBuildings = [];
    }

    getDashboardGameBuildings() {
        mongoConnect.findAllBuildings().then((creator) => {
            creator.forEach((doc) => {
                doc.buildings.forEach((_doc) => {
                    if (!_doc) {
                        return null
                    }
                    let index;
                    try
                    {
                        index = this.dashboardGameBuildings.findIndex(x => x._id.toHexString() === _doc._id.toHexString());
                    } catch (e) {
                        console.log(e);
                        index = -1;

                    }
                    if (index === -1) {
                        for (let style = 0; style < buildingsStyle.length; style++) {
                            if (buildingsStyle[style].title === _doc.title) {
                                _doc._hexColor = buildingsStyle[style].color.hex;
                            }
                        }
                        _doc['_creator'] = doc._creator;
                        mongoConnect.findPlayerId(doc._creator.toString()).then((player) => {
                            player.forEach((player) => {
                                _doc['_gender'] = player.gender;
                                this.dashboardGameBuildings.push(_doc);
                            });
                        });
                    }
                })
            })
        }, function (err) {
            console.error('The promise was rejected', err, err.stack);
        });
        return this.dashboardGameBuildings;
    }

    getDashboardGameBuildingsGender(filter, A, B, C) {
        for (let b = 0; b < this.dashboardGameBuildings.length; b++) {


            if (this.dashboardGameBuildings[b]._gender === filter) {
                this.dashboardGameBuildings[b]["radius"] = A;
            } else if ("all" === filter) {
                this.dashboardGameBuildings[b]["radius"] = B;
            } else {
                this.dashboardGameBuildings[b]["radius"] = C;
            }
        }
        return this.dashboardGameBuildings;
    }
}

module.exports = {DashboardGameBuildings};