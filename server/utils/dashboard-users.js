/**
 * Created by starsky on 5/12/17.
 */

const {Admin} = require('./../models/adminModel');

class DashboardUsers {
    constructor() {
        this.dashboardUsers = [];
    }

    addDashboardUser(id, email, room) {
        let adminEmail = email;
        let adminPassword = '123456';
        let admin = new Admin({
            email: email,
            password: '123456',
            _login: 1
        });
        admin.save().then((admin) => {
            return admin.generateAuthToken();
        }).catch((e) => {
            Admin.findByCredentials(adminEmail, adminPassword).then((admin) => {
                Admin.updateByToken(admin.tokens[0].token).then((update) => {
                });
            });

        });
        let index;
        let dashboardUser = {
            id,
            email,
            room
        };
        try {
            index = this.dashboardUsers.findIndex(x => x.email === dashboardUser.email);
            if (index === -1) {
                this.dashboardUsers.push(dashboardUser);
                return true;
            } else {
                return false;
            }
        } catch (e) {
            console.log(e);
        }
    }

    removeDashboardUser(id) {
        let dashboardUser = this.getDashboardUser(id);

        if (dashboardUser) {
            this.dashboardUsers = this.dashboardUsers.filter((dashboardUser) => dashboardUser.id !== id);
        }
        return dashboardUser;
    }

    getDashboardUser(id) {
        return this.dashboardUsers.filter((dashboardUser) => dashboardUser.id === id)[0];
    }

    getDashboardUserList(room) {
        let dashboardUsers = this.dashboardUsers.filter((dashboardUser) => dashboardUser.room === room);
        return dashboardUsers.map((dashboardUser) => dashboardUser.email);
    }
}

module.exports = {DashboardUsers};