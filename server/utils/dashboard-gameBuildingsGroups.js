/**
 * Created by starsky on 5/17/17.
 */
const _ = require('lodash');
let mongoConnect = require('./mongoConnect');

const {scoreStyle} = require('./database/scoreStyle');
const {buildingsStyle} = require('./database/buildingsStyle');

class DashboardGameBuildingsGroups {
    constructor() {
        this.dashboardGameBuildingsGroups = {
            accessibility: {
                all: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                male: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                female: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                undefine: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                }
            },
            productivity: {
                all: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                male: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                female: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                undefine: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                }
            },
            socialCulture: {
                all: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                male: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                female: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                undefine: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                }
            },
            ecology: {
                all: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                male: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                female: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                },
                undefine: {
                    titles: [],
                    counter: {},
                    _total: 0,
                    color: {
                        _general: scoreStyle.socialCulture._general.hex
                    }
                }
            }
        };
    };

    getDashboardGameBuildingsGroups() {
        mongoConnect.findAllBuildings().then((scores) => {
            console.info(`Starting to push ${Object.keys(scores).length} buildings groups`);
            scores.forEach((data) => {
                data.buildings.forEach((doc) => {
                    let index;
                    if (!doc) {
                        return null
                    }
                    mongoConnect.findPlayerId(data._creator).then((player) => {
                        player.forEach((player) => {
                            for (let style = 0; style < buildingsStyle.length; style++) {
                                if (buildingsStyle[style].title === doc.title) {
                                    let buildingScores = this.dashboardGameBuildingsGroups[buildingsStyle[style]._score._type];
                                    try {
                                        index = buildingScores.all.titles.findIndex(x => x === buildingsStyle[style].title);
                                        if (index === -1) {
                                            buildingScores.all.counter[buildingsStyle[style]._title] = 1;
                                            buildingScores.all._total = buildingScores.all._total + 1;
                                            buildingScores.all.color[buildingsStyle[style]._title] = buildingsStyle[style].color.hex;

                                            buildingScores[player.gender].counter[buildingsStyle[style]._title] = 1;
                                            buildingScores[player.gender]._total = buildingScores[player.gender]._total + 1;
                                            buildingScores[player.gender].color[buildingsStyle[style]._title] = buildingsStyle[style].color.hex;
                                        }
                                        else {
                                            if (!buildingScores[player.gender].counter[buildingsStyle[style]._title]) {
                                                buildingScores[player.gender].counter[buildingsStyle[style]._title] = 0;
                                                buildingScores[player.gender].color[buildingsStyle[style]._title] = buildingsStyle[style].color.hex;
                                            }
                                            if (!buildingScores.all.counter[buildingsStyle[style]._title]) {
                                                buildingScores.all.counter[buildingsStyle[style]._title] = 0;
                                                buildingScores.all.color[buildingsStyle[style]._title] = buildingsStyle[style].color.hex;
                                            }

                                            buildingScores.all.counter[buildingsStyle[style]._title] = buildingScores.all.counter[buildingsStyle[style]._title] + 1;
                                            buildingScores.all._total = buildingScores.all._total + 1;

                                            buildingScores[player.gender].counter[buildingsStyle[style]._title] = buildingScores[player.gender].counter[buildingsStyle[style]._title] + 1;
                                            buildingScores[player.gender]._total = buildingScores[player.gender]._total + 1;
                                        }

                                        buildingScores.all.titles.push(buildingsStyle[style].title);
                                        buildingScores[player.gender].titles.push(buildingsStyle[style].title);


                                    } catch (e) {
                                        console.log(buildingsStyle[style]._score._type, e);
                                    }
                                }
                            }
                        });
                    });
                });
            })
        }, function (err) {
            console.error('The promise was rejected', err, err.stack);
        });
        return this.dashboardGameBuildingsGroups;
    };

    getDashboardGameBuildingsScoresCounterScores(filter) {
        if (filter === 'all') {
            return {
                accessibility: {
                    // titles: this.dashboardGameBuildingsGroups.accessibility.all.titles,
                    counter: this.dashboardGameBuildingsGroups.accessibility.all.counter,
                    _total: this.dashboardGameBuildingsGroups.accessibility.all._total,
                    color: this.dashboardGameBuildingsGroups.accessibility.all.color
                },
                productivity: {
                    // titles: this.dashboardGameBuildingsGroups.productivity.all.titles,
                    counter: this.dashboardGameBuildingsGroups.productivity.all.counter,
                    _total: this.dashboardGameBuildingsGroups.productivity.all._total,
                    color: this.dashboardGameBuildingsGroups.productivity.all.color
                },
                socialCulture: {
                    // titles: this.dashboardGameBuildingsGroups.socialCulture.all.titles,
                    counter: this.dashboardGameBuildingsGroups.socialCulture.all.counter,
                    _total: this.dashboardGameBuildingsGroups.socialCulture.all._total,
                    color: this.dashboardGameBuildingsGroups.socialCulture.all.color
                },
                ecology: {
                    // titles: this.dashboardGameBuildingsGroups.ecology.all.titles,
                    counter: this.dashboardGameBuildingsGroups.ecology.all.counter,
                    _total: this.dashboardGameBuildingsGroups.ecology.all._total,
                    color: this.dashboardGameBuildingsGroups.ecology.all.color
                }
            };
        } else if (filter === 'male') {
            return {
                accessibility: {
                    // titles: this.dashboardGameBuildingsGroups.accessibility.male.titles,
                    counter: this.dashboardGameBuildingsGroups.accessibility.male.counter,
                    _total: this.dashboardGameBuildingsGroups.accessibility.male._total,
                    color: this.dashboardGameBuildingsGroups.accessibility.male.color
                },
                productivity: {
                    // titles: this.dashboardGameBuildingsGroups.productivity.male.titles,
                    counter: this.dashboardGameBuildingsGroups.productivity.male.counter,
                    _total: this.dashboardGameBuildingsGroups.productivity.male._total,
                    color: this.dashboardGameBuildingsGroups.productivity.male.color
                },
                socialCulture: {
                    // titles: this.dashboardGameBuildingsGroups.socialCulture.male.titles,
                    counter: this.dashboardGameBuildingsGroups.socialCulture.male.counter,
                    _total: this.dashboardGameBuildingsGroups.socialCulture.male._total,
                    color: this.dashboardGameBuildingsGroups.socialCulture.male.color
                },
                ecology: {
                    // titles: this.dashboardGameBuildingsGroups.ecology.male.titles,
                    counter: this.dashboardGameBuildingsGroups.ecology.male.counter,
                    _total: this.dashboardGameBuildingsGroups.ecology.male._total,
                    color: this.dashboardGameBuildingsGroups.ecology.male.color
                }
            }
        } else if (filter === 'female') {
            return {
                accessibility: {
                    // titles: this.dashboardGameBuildingsGroups.accessibility.female.titles,
                    counter: this.dashboardGameBuildingsGroups.accessibility.female.counter,
                    _total: this.dashboardGameBuildingsGroups.accessibility.female._total,
                    color: this.dashboardGameBuildingsGroups.accessibility.female.color
                },
                productivity: {
                    // titles: this.dashboardGameBuildingsGroups.productivity.female.titles,
                    counter: this.dashboardGameBuildingsGroups.productivity.female.counter,
                    _total: this.dashboardGameBuildingsGroups.productivity.female._total,
                    color: this.dashboardGameBuildingsGroups.productivity.female.color
                },
                socialCulture: {
                    // titles: this.dashboardGameBuildingsGroups.socialCulture.female.titles,
                    counter: this.dashboardGameBuildingsGroups.socialCulture.female.counter,
                    _total: this.dashboardGameBuildingsGroups.socialCulture.female._total,
                    color: this.dashboardGameBuildingsGroups.socialCulture.female.color
                },
                ecology: {
                    // titles: this.dashboardGameBuildingsGroups.ecology.female.titles,
                    counter: this.dashboardGameBuildingsGroups.ecology.female.counter,
                    _total: this.dashboardGameBuildingsGroups.ecology.female._total,
                    color: this.dashboardGameBuildingsGroups.ecology.female.color
                }
            }
        }
    }
}

module.exports = {DashboardGameBuildingsGroups};
