/**
 * Created by starsky on 4/25/17.
 */
let socket = io();

$(document).ready(function () {
    let filter = "none";
    $(".filter").click(function () {
        filter = $(this).attr("data");
        socket.emit('allData', filter);
    });
});

socket.on('connect', function () {
    let params = jQuery.deparam(window.location.search);
    socket.emit('join', params, function (err) {
        if (err) {
            alert(err);
            window.location.href = '/';
        } else {
            console.log('No error');
        }
    });
});

socket.on('update_DashboardUsers', function (DashboardUsers) {
    let ol = jQuery('<ul></ul>');
    DashboardUsers.forEach(function (dashboardUser) {
        ol.append(jQuery('<li></li>').text(dashboardUser))
    });
    jQuery('#dashboardUsers').html(ol);

});

socket.on('update_DashboardBuildings', function (buildings) {

    let _buildings = function (i, callback) {
        callback(null, i);
    };
    let _svgRemove = function (callback) {
        if (jQuery('#superBarrioMap').find('svg').length === 1) {
            jQuery('#superBarrioMap').find('svg').remove();
        }
        callback(null);
    };
    let plotMap = function (error, results) {
        map('#superBarrioMap', results[0]);
    };

    queue()
        .defer(_buildings, buildings)
        .defer(_svgRemove)
        .awaitAll(plotMap);

    function map(div, buildings) {
        let factorY = 5.2;
        let originY = -100;
        let factorX = 5;
        let originX = -40;

        let width =  80 * factor;
        let height = 120 * factor;
        let svg = d3.select(div).append('svg')
            .attr('id', 'superBarrio-gameData')
            .attr('width', width)
            .attr('height', height);
        svg.selectAll('loc')
            .data(buildings)
            .enter()
            .append('circle')
            .attr('class', 'loc')
            .attr('cx', function (d, i) {
                try {
                    return (d.x - (originX)) * factorX;
                } catch (e) {
                    console.log(d.x);
                }
            })
            .attr('cy', function (d, i) {
                try {
                    return (d.y - (originY)) * factorY;
                } catch (e) {
                    console.log(d.y);
                }
            })
            .attr('r', function (d, i) {
                try {
                    return d.radius;
                } catch (e) {
                    console.log(d.radius);
                }

            })
            .attr('fill', function (d, i) {
                try {
                    return d._hexColor;

                } catch (e) {
                    console.log(d);
                }
            })
            .attr('fill-opacity', '0.4');
    }

});

socket.on('update_DashboardScores', function (data, filter) {

    let _data = function (i, callback) {
        callback(null, i);
    };
    let _svgRemove = function (callback) {
        if (jQuery('#superBarrioScores__ecology').find('svg').length === 1) {
            jQuery('#superBarrioScores__ecology').find('svg').remove();
        }
        if (jQuery('#superBarrioScores__productivity').find('svg').length === 1) {
            jQuery('#superBarrioScores__productivity').find('svg').remove();
        }
        if (jQuery('#superBarrioScores__socialCulture').find('svg').length === 1) {
            jQuery('#superBarrioScores__socialCulture').find('svg').remove();
        }
        if (jQuery('#superBarrioScores__accessibility').find('svg').length === 1) {
            jQuery('#superBarrioScores__accessibility').find('svg').remove();
        }
        callback(null);
    };
    let _addLegend = function (f, d, callback) {

        let ecology_ol = jQuery('<ul></ul>');
        ecology_ol.append(jQuery('<li></li>').text(`Dataset:`));
        ecology_ol.append(jQuery('<li class="legend_bold"></li>').text(`${f}`));
        ecology_ol.append(jQuery('<li></li>').text(`Objects:`));
        ecology_ol.append(jQuery('<li class="legend_bold"></li>').text(`${d.ecology._total}`));
        jQuery(`#dashboard_statistics__title__legend__ecology`).html(ecology_ol);


        let productivity_ol = jQuery('<ul></ul>');
        productivity_ol.append(jQuery('<li></li>').text(`Dataset:`));
        productivity_ol.append(jQuery('<li class="legend_bold"></li>').text(`${f}`));
        productivity_ol.append(jQuery('<li></li>').text(`Objects:`));
        productivity_ol.append(jQuery('<li class="legend_bold"></li>').text(`${d.productivity._total}`));
        jQuery(`#dashboard_statistics__title__legend__productivity`).html(productivity_ol);

        let socialCulture_ol = jQuery('<ul></ul>');
        socialCulture_ol.append(jQuery('<li></li>').text(`Dataset:`));
        socialCulture_ol.append(jQuery('<li class="legend_bold"></li>').text(`${f}`));
        socialCulture_ol.append(jQuery('<li></li>').text(`Objects:`));
        socialCulture_ol.append(jQuery('<li class="legend_bold"></li>').text(`${d.socialCulture._total}`));
        jQuery(`#dashboard_statistics__title__legend__socialCulture`).html(socialCulture_ol);

        let accessibility_ol = jQuery('<ul></ul>');
        accessibility_ol.append(jQuery('<li></li>').text(`Dataset:`));
        accessibility_ol.append(jQuery('<li class="legend_bold"></li>').text(`${f}`));
        accessibility_ol.append(jQuery('<li></li>').text(`Objects:`));
        accessibility_ol.append(jQuery('<li class="legend_bold"></li>').text(`${d.accessibility._total}`));
        jQuery(`#dashboard_statistics__title__legend__accessibility`).html(accessibility_ol);



        let domain = Object.keys(d);
        domain.forEach((data) => {
            console.log(d[data]);
        });


        callback(null);
    };
    let plotData = function (error, results) {
        pieChart('ecology', '#superBarrioScores__ecology', results[0]);
        pieChart('productivity', '#superBarrioScores__productivity', results[0]);
        pieChart('socialCulture', '#superBarrioScores__socialCulture', results[0]);
        pieChart('accessibility', '#superBarrioScores__accessibility', results[0]);
    };

    queue()
        .defer(_data, data)
        .defer(_svgRemove)
        .defer(_addLegend, filter, data)
        .awaitAll(plotData);

    function pieChart(group, div, scores) {
        let domain = Object.keys(scores[group].counter);
        let range = [];
        for (let i = 0; i < domain.length; i++) {
            let color = scores[group].color[domain[i]];
            range.push(color);
        }
        let color = d3.scale.ordinal()
            .domain(domain)
            .range(range);
        let score_data = scoresData(scores, group, color);
        let svg = d3.select(div)
            .append("svg")
            .append("g");
        svg.append("g")
            .attr("class", "slices");
        svg.append("g")
            .attr("class", "labels");
        svg.append("g")
            .attr("class", "counters");
        svg.append("g")
            .attr("class", "lines");
        plot(score_data, svg, color);
    }

    function scoresData(_scores, _group, _color) {
        let labels = _color.domain();
        return labels.map(function (label) {
            return {
                label: `${label}:`,
                counter: `${_scores[_group].counter[label]} units`,
                value: _scores[_group].counter[label],
                percentage: `${Math.round((100 / _scores[_group]._total ) * _scores[_group].counter[label] * 100.0) / 100.0}%`
            }
        });
    }

    function plot(data, svg, color) {
        let width = 960 * 0.3,
            height = 500 * 0.3,
            radius = (Math.min(width, height) / 2);
        svg.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
        let arc = d3.svg.arc()
            .outerRadius(radius * 0.8)
            .innerRadius(radius * 0.4);
        let outerArc = d3.svg.arc()
            .innerRadius(radius * 0.9)
            .outerRadius(radius * 0.9);
        let pie = d3.layout.pie()
            .sort(null)
            .value(function (d) {
                return d.value;
            });
        let key_ = function (d) {
            return d.data.label;
        };
        /* ------- PIE SLICES -------*/
        let slice = svg.select(".slices").selectAll("path.slice")
            .data(pie(data), key_);
        slice.enter()
            .insert("path")
            .style("fill", function (d) {
                return color(d.data.label);
            })
            .attr("class", "slice");
        slice
            .transition().duration(1000)
            .attrTween("d", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    return arc(interpolate(t));
                };
            });
        slice.exit()
            .remove();
        /* ------- TEXT LABELS -------*/
        let textLabel = svg.select(".labels").selectAll("text")
            .data(pie(data), key_);
        textLabel.enter()
            .append("text")
            .attr("dy", ".10em")
            .attr("style", "font-size:0.7em;")
            .text(function (d) {
                return d.data.label;
            });
        function midAngle(d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        }

        textLabel.transition().duration(1000)
            .attrTween("transform", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    let d2 = interpolate(t);
                    let pos = outerArc.centroid(d2);
                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                    return "translate(" + pos + ")";
                };
            })
            .styleTween("text-anchor", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    let d2 = interpolate(t);
                    return midAngle(d2) < Math.PI ? "start" : "end";
                };
            });
        textLabel.exit()
            .remove();
        /* ------- TEXT COUNTER -------*/
        let textCounter = svg.select(".counters").selectAll("text")
            .data(pie(data), key_);
        textCounter.enter()
            .append("text")
            .attr("dy", "1.2em")
            .attr("style", "font-size:0.7em;")
            .text(function (d) {
                return d.data.percentage;
            });
        function midAngle(d) {
            return d.startAngle + (d.endAngle - d.startAngle) / 2;
        }

        textCounter.transition().duration(1000)
            .attrTween("transform", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    let d2 = interpolate(t);
                    let pos = outerArc.centroid(d2);
                    pos[0] = radius * (midAngle(d2) < Math.PI ? 1 : -1);
                    return "translate(" + pos + ")";
                };
            })
            .styleTween("text-anchor", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    let d2 = interpolate(t);
                    return midAngle(d2) < Math.PI ? "start" : "end";
                };
            });
        textCounter.exit()
            .remove();
        /* ------- SLICE TO TEXT POLYLINES -------*/
        let polyline = svg.select(".lines").selectAll("polyline")
            .data(pie(data), key_);
        polyline.enter()
            .append("polyline");
        polyline.transition().duration(1000)
            .attrTween("points", function (d) {
                this._current = this._current || d;
                let interpolate = d3.interpolate(this._current, d);
                this._current = interpolate(0);
                return function (t) {
                    let d2 = interpolate(t);
                    let pos = outerArc.centroid(d2);
                    pos[0] = radius * 0.95 * (midAngle(d2) < Math.PI ? 1 : -1);
                    return [arc.centroid(d2), outerArc.centroid(d2), pos];
                };
            })
            .attr('fill', "#FFFFFF")
            .attr('fill-opacity', "0.0")
            .attr('stroke', '#000000')
            .attr('stroke-width', '1');
        polyline.exit()
            .remove();
    }
});